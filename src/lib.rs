#![allow(dead_code)]

pub mod analyse;
pub mod ast;
pub mod builtin;
pub mod const_eval;
pub mod dep_analyse;
pub mod env;
pub mod generate;
pub mod ident;
pub mod lex;
pub mod lit;
pub mod parse;
pub mod swizzle;
pub mod token;
pub mod ty;
pub mod ty_check;
pub mod util;
pub mod val;
